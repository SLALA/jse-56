package ru.t1.strelcov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.strelcov.tm.dto.request.UserLogoutRequest;

@Component
public final class UserLogoutCommand extends AbstractUserCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "logout";
    }

    @NotNull
    @Override
    public String description() {
        return "Logout.";
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT]");
        authEndpoint.logout(new UserLogoutRequest(getToken()));
    }

}
