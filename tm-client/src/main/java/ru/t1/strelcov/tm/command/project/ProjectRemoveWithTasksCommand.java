package ru.t1.strelcov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.strelcov.tm.dto.model.ProjectDTO;
import ru.t1.strelcov.tm.dto.request.ProjectRemoveWithTasksRequest;
import ru.t1.strelcov.tm.util.TerminalUtil;

@Component
public final class ProjectRemoveWithTasksCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-remove-with-tasks";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove project with bound tasks.";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT WITH PROJECTS]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final ProjectDTO project = projectEndpoint.removeWithTasksProject(new ProjectRemoveWithTasksRequest(getToken(), id)).getProject();
    }

}
