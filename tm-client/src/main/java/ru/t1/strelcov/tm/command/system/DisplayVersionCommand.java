package ru.t1.strelcov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.strelcov.tm.command.AbstractCommand;

@Component
public final class DisplayVersionCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-v";
    }

    @NotNull
    @Override
    public String name() {
        return "version";
    }

    @NotNull
    @Override
    public String description() {
        return "Display program version.";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println(propertyService.getVersion());
    }

}
