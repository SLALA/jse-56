package ru.t1.strelcov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.strelcov.tm.dto.model.TaskDTO;
import ru.t1.strelcov.tm.dto.request.TaskListRequest;
import ru.t1.strelcov.tm.dto.request.TaskListSortedRequest;
import ru.t1.strelcov.tm.enumerated.SortType;
import ru.t1.strelcov.tm.exception.system.IncorrectSortOptionException;
import ru.t1.strelcov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

@Component
public final class TaskListCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-list";
    }

    @NotNull
    @Override
    public String description() {
        return "List tasks.";
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT FIELD " + Arrays.toString(SortType.values()) + ":");
        @NotNull List<TaskDTO> tasks;
        final String sort = TerminalUtil.nextLine();
        if (sort.isEmpty()) tasks = taskEndpoint.listTask(new TaskListRequest(getToken())).getList();
        else {
            if (SortType.isValidByName(sort)) {
                @NotNull final SortType sortType = SortType.valueOf(sort);
                tasks = taskEndpoint.listSortedTask(new TaskListSortedRequest(getToken(), sort)).getList();
                System.out.println(sortType.getDisplayName());
            } else
                throw new IncorrectSortOptionException(sort);
        }
        int index = 1;
        for (final TaskDTO task : tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
    }

}
