package ru.t1.strelcov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.dto.model.UserDTO;

@Setter
@Getter
@NoArgsConstructor
public abstract class AbstractUserResponse extends AbstractResultResponse {

    @NotNull
    private UserDTO user;

    public AbstractUserResponse(@NotNull final UserDTO user) {
        this.user = user;
    }

}
