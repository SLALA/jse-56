package ru.t1.strelcov.tm.bootstrap;

import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.activemq.broker.BrokerService;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.strelcov.tm.api.service.ILoggerService;
import ru.t1.strelcov.tm.api.service.IPropertyService;
import ru.t1.strelcov.tm.api.service.dto.IProjectDTOService;
import ru.t1.strelcov.tm.api.service.dto.ITaskDTOService;
import ru.t1.strelcov.tm.api.service.dto.IUserDTOService;
import ru.t1.strelcov.tm.component.Backup;
import ru.t1.strelcov.tm.endpoint.AbstractEndpoint;
import ru.t1.strelcov.tm.enumerated.Role;
import ru.t1.strelcov.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Comparator;

@Component
@Getter
public final class Bootstrap {

    @Autowired
    @NotNull
    private IPropertyService propertyService;

    @Autowired
    @NotNull
    private ILoggerService loggerService;

    @Autowired
    private AbstractEndpoint[] endpoints;

    @Autowired
    private BrokerService broker;

    @Autowired
    private Backup backup;

    @Autowired
    @NotNull
    private ITaskDTOService taskService;

    @Autowired
    @NotNull
    private IProjectDTOService projectService;

    @Autowired
    @NotNull
    private IUserDTOService userService;

    private void initUsers() {
        userService.clear();
        userService.add("admin", "admin", Role.ADMIN);
    }

    private void initData() {
        projectService.clear();
        taskService.clear();
        initUsers();
        @Nullable String userId = userService.findByLogin("admin").getId();
        @Nullable String projectId = projectService.add(userId, "p1", "p1").getId();
        projectService.add(userId, "p2", "p2");
        taskService.add(userId, "t1", "t1").setProjectId(projectId);
        taskService.add(userId, "t2", "t2").setProjectId(projectId);
        projectService.add(userId, "pa2", "pa2");
        taskService.add(userId, "a1", "a1").setProjectId(projectId);
        taskService.add(userId, "a2", "a2").setProjectId(projectId);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void initEndpoints() {
        Arrays.stream(endpoints)
                .sorted(Comparator.comparing(c -> c.getClass().getName()))
                .forEach(this::registerEndpoint);
    }

    public void run() {
        initPID();
        if("enabled".equals(propertyService.getInitDataStatus()))
            initData();
        initEndpoints();
        displayWelcome();
    }

    public void displayWelcome() {
        loggerService.info("** TASK MANAGER SERVER STARTED **");
    }

    private void registerEndpoint(@Nullable final AbstractEndpoint endpoint) {
        if (endpoint == null) return;
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final Integer port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String wsdlURL = "http://" + host + ":" + port + "/" + name + "?wsdl";
        System.out.println(wsdlURL);
        Endpoint.publish(wsdlURL, endpoint);
    }

}
