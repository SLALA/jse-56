package ru.t1.strelcov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.strelcov.tm.api.endpoint.IDataEndpoint;
import ru.t1.strelcov.tm.dto.model.SessionDTO;
import ru.t1.strelcov.tm.dto.request.*;
import ru.t1.strelcov.tm.dto.response.*;
import ru.t1.strelcov.tm.enumerated.Role;
import ru.t1.strelcov.tm.service.DataService;

import javax.jws.WebService;

@Controller
@WebService(endpointInterface = "ru.t1.strelcov.tm.api.endpoint.IDataEndpoint")
@NoArgsConstructor
public final class DataEndpoint extends AbstractEndpoint implements IDataEndpoint {

    @Autowired
    @NotNull
    private DataService dataService;

    @NotNull
    @Override
    public DataBase64LoadResponse loadBase64Data(@NotNull DataBase64LoadRequest request) {
        @NotNull final SessionDTO session = check(request, Role.ADMIN);
        dataService.loadDataBase64();
        return new DataBase64LoadResponse();
    }

    @NotNull
    @Override
    public DataBase64SaveResponse saveBase64Data(@NotNull DataBase64SaveRequest request) {
        @NotNull final SessionDTO session = check(request, Role.ADMIN);
        dataService.saveDataBase64();
        return new DataBase64SaveResponse();
    }

    @NotNull
    @Override
    public DataBinaryLoadResponse loadBinaryData(@NotNull DataBinaryLoadRequest request) {
        @NotNull final SessionDTO session = check(request, Role.ADMIN);
        dataService.loadDataBinary();
        return new DataBinaryLoadResponse();
    }

    @NotNull
    @Override
    public DataBinarySaveResponse saveBinaryData(@NotNull DataBinarySaveRequest request) {
        @NotNull final SessionDTO session = check(request, Role.ADMIN);
        dataService.saveDataBinary();
        return new DataBinarySaveResponse();
    }

    @NotNull
    @Override
    public DataJsonJAXBSaveResponse saveJsonJAXBData(@NotNull DataJsonJAXBSaveRequest request) {
        @NotNull final SessionDTO session = check(request, Role.ADMIN);
        dataService.saveDataJsonJAXB();
        return new DataJsonJAXBSaveResponse();
    }

    @NotNull
    @Override
    public DataJsonJAXBLoadResponse loadJsonJAXBData(@NotNull DataJsonJAXBLoadRequest request) {
        @NotNull final SessionDTO session = check(request, Role.ADMIN);
        dataService.loadDataJsonJAXB();
        return new DataJsonJAXBLoadResponse();
    }

    @NotNull
    @Override
    public DataJsonFasterXMLLoadResponse loadJsonFasterXMLData(@NotNull DataJsonFasterXMLLoadRequest request) {
        @NotNull final SessionDTO session = check(request, Role.ADMIN);
        dataService.loadDataJsonFasterXml();
        return new DataJsonFasterXMLLoadResponse();
    }

    @NotNull
    @Override
    public DataXmlFasterXMLLoadResponse loadXmlFasterXMLData(@NotNull DataXmlFasterXMLLoadRequest request) {
        @NotNull final SessionDTO session = check(request, Role.ADMIN);
        dataService.loadDataXmlFasterXml();
        return new DataXmlFasterXMLLoadResponse();
    }

    @NotNull
    @Override
    public DataYamlFasterXMLSaveResponse saveYamlFasterXMLData(@NotNull DataYamlFasterXMLSaveRequest request) {
        @NotNull final SessionDTO session = check(request, Role.ADMIN);
        dataService.saveDataYamlFasterXml();
        return new DataYamlFasterXMLSaveResponse();
    }

    @NotNull
    @Override
    public DataJsonFasterXMLSaveResponse saveJsonFasterXMLData(@NotNull DataJsonFasterXMLSaveRequest request) {
        @NotNull final SessionDTO session = check(request, Role.ADMIN);
        dataService.saveDataJsonFasterXml();
        return new DataJsonFasterXMLSaveResponse();
    }

    @NotNull
    @Override
    public DataXmlFasterXMLSaveResponse saveXmlFasterXMLData(@NotNull DataXmlFasterXMLSaveRequest request) {
        @NotNull final SessionDTO session = check(request, Role.ADMIN);
        dataService.saveDataXmlFasterXml();
        return new DataXmlFasterXMLSaveResponse();
    }

    @NotNull
    @Override
    public DataYamlFasterXMLLoadResponse loadYamlFasterXMLData(@NotNull DataYamlFasterXMLLoadRequest request) {
        @NotNull final SessionDTO session = check(request, Role.ADMIN);
        dataService.loadDataYamlFasterXml();
        return new DataYamlFasterXMLLoadResponse();
    }

    @NotNull
    @Override
    public DataXmlJAXBLoadResponse loadXmlJAXBData(@NotNull DataXmlJAXBLoadRequest request) {
        @NotNull final SessionDTO session = check(request, Role.ADMIN);
        dataService.loadDataXmlJAXB();
        return new DataXmlJAXBLoadResponse();
    }

    @NotNull
    @Override
    public DataXmlJAXBSaveResponse saveXmlJAXBData(@NotNull DataXmlJAXBSaveRequest request) {
        @NotNull final SessionDTO session = check(request, Role.ADMIN);
        dataService.saveDataXmlJAXB();
        return new DataXmlJAXBSaveResponse();
    }

}
