package ru.t1.strelcov.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.strelcov.tm.api.repository.dto.IProjectDTORepository;
import ru.t1.strelcov.tm.api.repository.dto.ITaskDTORepository;
import ru.t1.strelcov.tm.api.service.dto.IProjectDTOService;
import ru.t1.strelcov.tm.dto.model.ProjectDTO;
import ru.t1.strelcov.tm.exception.empty.EmptyIdException;
import ru.t1.strelcov.tm.exception.entity.AccessDeniedException;
import ru.t1.strelcov.tm.exception.entity.EntityNotFoundException;
import ru.t1.strelcov.tm.repository.dto.ProjectDTORepository;
import ru.t1.strelcov.tm.repository.dto.TaskDTORepository;

import javax.persistence.EntityManager;
import java.util.Optional;

@Service
public final class ProjectDTOService extends AbstractBusinessDTOService<ProjectDTO> implements IProjectDTOService {

    @NotNull
    public IProjectDTORepository getRepository(@NotNull final EntityManager entityManager) {
        return context.getBean(ProjectDTORepository.class, entityManager);
    }

    @NotNull
    public EntityManager getEntityManager() {
        return context.getBean(EntityManager.class);
    }

    @NotNull
    public ITaskDTORepository getTaskRepository(@NotNull final EntityManager entityManager) {
        return context.getBean(TaskDTORepository.class, entityManager);
    }

    @NotNull
    @Override
    public ProjectDTO add(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(name).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        final ProjectDTO project;
        if (Optional.ofNullable(description).filter((i) -> !i.isEmpty()).isPresent())
            project = new ProjectDTO(userId, name, description);
        else
            project = new ProjectDTO(userId, name);
        add(project);
        return project;
    }

    @SneakyThrows
    @NotNull
    @Override
    public ProjectDTO removeProjectWithTasksById(@Nullable final String userId, @Nullable final String projectId) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(projectId).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final ITaskDTORepository taskRepository = getTaskRepository(entityManager);
        @NotNull final IProjectDTORepository projectRepository = getRepository(entityManager);
        try {
            entityManager.getTransaction().begin();
            taskRepository.removeAllByProjectId(userId, projectId);
            @NotNull final ProjectDTO project = Optional.ofNullable(projectRepository.removeById(userId, projectId)).orElseThrow(EntityNotFoundException::new);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
