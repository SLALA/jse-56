package ru.t1.strelcov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.strelcov.tm.api.repository.model.IProjectRepository;
import ru.t1.strelcov.tm.model.Project;

import javax.persistence.EntityManager;

@Repository
@Scope("prototype")
public final class ProjectRepository extends AbstractBusinessRepository<Project> implements IProjectRepository {

    @Autowired
    public ProjectRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    @NotNull
    public Class<Project> getClazz() {
        return Project.class;
    }

}
